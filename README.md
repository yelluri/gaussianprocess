Data Mining Project for doing Preference Learning using Gaussian Process.

Requires pyGPs package.

Implementations of Game simulations for Othello and checkers are available given a heuristic vector.

Thus creating a dataset on the fly by game simulations and learning the preference / winners in a heuristic pair.

This model, capable of predicting the winner given a pair of heuristics is now optimized to produce a pair with very strong and very weak heuristic.